// 1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// setTimeout позволяет вызвать функцию один раз через определённый интервал времени.
// setInterval позволяет вызывать функцию регулярно, повторяя вызов через определённый интервал времени.

// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Функция сработает после выполнения всего текущего кода.

// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Функция clearInterval() необходима для сброса значений setTimeout() і setInterval(), если они больше не используются.


const image = document.querySelectorAll('.image-to-show');
const stop = document.querySelector('.stop');
const play = document.querySelector('.play');

let COUNT = 0;
let interval;

function sliderImage() {
    interval = setInterval(() => {
        if (COUNT === image.length - 1) {
            image[COUNT].classList.toggle('show');
            COUNT = -1;
            image[COUNT + 1].classList.toggle('show');
        } else {
            image[COUNT].classList.toggle('show');
            image[COUNT + 1].classList.toggle('show');
        }
        COUNT++;
    }, 3000);
}

sliderImage();


stop.addEventListener('click', e => {
    clearInterval(interval);
});

play.addEventListener('click', e => {
    sliderImage();
});


